package com.swtec.domain.interactor

import com.swtec.domain.model.Contact
import com.swtec.domain.repository.ContactsRepository
import javax.inject.Inject

class GetContactById @Inject constructor(private val contactsRepository: ContactsRepository) :
    UseCase<Long, Contact?>() {
    override suspend fun performAction(param: Long) = contactsRepository.getContactById(param)
}