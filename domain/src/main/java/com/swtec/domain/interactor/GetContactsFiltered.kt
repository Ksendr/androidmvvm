package com.swtec.domain.interactor

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.swtec.domain.model.Contact
import com.swtec.domain.repository.ContactsRepository
import javax.inject.Inject

class GetContactsFiltered @Inject constructor(private val contactsRepository: ContactsRepository) :
    UseCase<GetContactsFiltered.Param, LiveData<PagingData<Contact>>>() {
    override suspend fun performAction(param: Param) = contactsRepository.getContactsFiltered(param.pageSize, param.filter)

    data class Param(val pageSize: Int, val filter: String)
}