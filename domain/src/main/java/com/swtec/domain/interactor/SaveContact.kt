package com.swtec.domain.interactor

import com.swtec.domain.model.Contact
import com.swtec.domain.repository.ContactsRepository
import javax.inject.Inject

class SaveContact @Inject constructor(private val contactsRepository: ContactsRepository) :
    UseCase<Contact, Long>() {
    override suspend fun performAction(param: Contact) = contactsRepository.saveContact(param)
}