package com.swtec.domain.interactor

import com.swtec.domain.repository.ContactsRepository
import javax.inject.Inject

class DeleteContact @Inject constructor(private val contactsRepository: ContactsRepository) :
    UseCase<Long, Unit>() {
    override suspend fun performAction(param: Long) = contactsRepository.deleteContact(param)
}