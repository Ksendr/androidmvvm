package com.swtec.domain.interactor

import android.net.Uri
import com.swtec.domain.repository.ContactsRepository
import javax.inject.Inject

class ExportContacts @Inject constructor(private val contactsRepository: ContactsRepository) :
    UseCase<Uri, Unit>() {
    override suspend fun performAction(param: Uri) = contactsRepository.exportContacts(param)
}