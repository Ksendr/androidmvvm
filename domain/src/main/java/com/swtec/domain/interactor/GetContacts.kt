package com.swtec.domain.interactor

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.swtec.domain.model.Contact
import com.swtec.domain.repository.ContactsRepository
import javax.inject.Inject

class GetContacts @Inject constructor(private val contactsRepository: ContactsRepository) :
    UseCase<Int, LiveData<PagingData<Contact>>>() {
    override suspend fun performAction(param: Int) = contactsRepository.getContactsPaging(param)
}