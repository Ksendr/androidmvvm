package com.swtec.domain.repository

interface Repository {
    fun clear()
}