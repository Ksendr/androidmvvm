package com.swtec.contacts.ui.viewcontact

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.swtec.contacts.databinding.ViewContactFragmentBinding
import com.swtec.contacts.di.Injectable
import com.swtec.contacts.vo.State
import javax.inject.Inject

class ViewContactFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: ViewContactViewModel by lazy {
        ViewModelProvider(this, viewModelFactory)[ViewContactViewModel::class.java]
    }

    private val params by navArgs<ViewContactFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel.setContact(params.contactId)
        val binding = ViewContactFragmentBinding.inflate(inflater)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.contact.observe(viewLifecycleOwner, Observer {
            if (it == null) {
                findNavController().navigate(ViewContactFragmentDirections.editContact())
            }
        })

        viewModel.deletedEvent.observe(viewLifecycleOwner, Observer {
            if(it.data is State.Success) {
                findNavController().popBackStack()
            }
        })

        binding.editButton.setOnClickListener {
            findNavController().navigate(ViewContactFragmentDirections.editContact(params.contactId))
        }

        binding.phoneImageView.setOnClickListener {
            call()
        }

        setHasOptionsMenu(true)
        return binding.root
    }

    private fun call() {
        viewModel.contact.value?.phone?.let {
            val number = Uri.parse("tel:$it")
            val callIntent = Intent(Intent.ACTION_DIAL, number)
            context?.startActivity(callIntent)
        }
    }

}
