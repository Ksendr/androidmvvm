package com.swtec.contacts.ui.editcontact

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.swtec.contacts.R
import com.swtec.contacts.databinding.EditContactFragmentBinding
import com.swtec.contacts.di.Injectable
import com.swtec.contacts.ui.Validators
import com.swtec.contacts.vo.State
import javax.inject.Inject

class EditContactFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: EditContactViewModel by lazy {
        ViewModelProvider(this, viewModelFactory)[EditContactViewModel::class.java]
    }

    private val params by navArgs<EditContactFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel.setContact(params.contactId)
        val binding = EditContactFragmentBinding.inflate(inflater)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.firstnameError.observe(viewLifecycleOwner, Observer {
            binding.firstnameEditText.error =
                if (it == null) null else resources.getString(R.string.required_error)
        })
        viewModel.phoneError.observe(viewLifecycleOwner, Observer {
            binding.phoneEditText.error = getError(it, R.string.phone_error)
        })
        viewModel.emailError.observe(viewLifecycleOwner, Observer {
            binding.emailEditText.error = getError(it, R.string.email_error)
        })
        viewModel.savedEvent.observe(viewLifecycleOwner, Observer {
            val data = it.data
            if(data is State.Success) {
                findNavController().navigate(EditContactFragmentDirections.viewContact(data.value))
            }
        })
        binding.cancelButton.setOnClickListener {
            findNavController().popBackStack()
        }

        setHasOptionsMenu(true)
        return binding.root
    }

    private fun getError(error: Validators.ValidationError?, @StringRes patternErrorRestId: Int): String? {
        return when (error) {
            Validators.ValidationError.REQUIRED -> resources.getString(R.string.required_error)
            Validators.ValidationError.PATTERN -> resources.getString(patternErrorRestId)
            else -> null
        }
    }
}
