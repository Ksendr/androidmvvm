package com.swtec.contacts.di.module

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.swtec.data.db.ContactDao
import com.swtec.data.db.ContactsDb
import com.swtec.data.filesystem.FileSystemService
import com.swtec.domain.repository.ContactsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {
    @Provides
    @Singleton
    fun context(app: Application): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideContactsRepository(contactsRepository: com.swtec.data.repository.ContactsRepository): ContactsRepository =
        contactsRepository

    @Singleton
    @Provides
    fun provideDb(app: Application): ContactsDb {
        return Room
            .databaseBuilder(app, ContactsDb::class.java, "contacts.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideContactDao(db: ContactsDb): ContactDao = db.contactDao()

    @Singleton
    @Provides
    fun provideFileSystemService(context: Context): FileSystemService = FileSystemService(context)
}
