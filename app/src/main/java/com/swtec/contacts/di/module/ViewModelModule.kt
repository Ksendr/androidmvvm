package com.swtec.contacts.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.swtec.contacts.di.ViewModelFactory
import com.swtec.contacts.di.ViewModelKey
import com.swtec.contacts.ui.contacts.ContactListViewModel
import com.swtec.contacts.ui.editcontact.EditContactViewModel
import com.swtec.contacts.ui.viewcontact.ViewContactViewModel

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ContactListViewModel::class)
    abstract fun bindContactListViewModel(contactListViewModel: ContactListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditContactViewModel::class)
    abstract fun bindEditContactViewModel(editContactViewModel: EditContactViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ViewContactViewModel::class)
    abstract fun bindViewContactViewModel(viewContactViewModel: ViewContactViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
