package com.swtec.contacts.di.module

import com.swtec.contacts.ui.contacts.ContactListFragment
import com.swtec.contacts.ui.editcontact.EditContactFragment
import com.swtec.contacts.ui.viewcontact.ViewContactFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeContactListFragment(): ContactListFragment

    @ContributesAndroidInjector
    abstract fun contributeEditContactFragment(): EditContactFragment

    @ContributesAndroidInjector
    abstract fun contributeViewContactFragment(): ViewContactFragment
}
